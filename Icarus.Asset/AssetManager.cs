﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace Icarus.Asset
{
    public class AssetManager
    {
        public AssetManager()
        {
            LoadAssets(Path.Combine("Resources"));
        }

        private void LoadAssets(string _directory)
        {
            foreach (var file in Directory.GetFiles(_directory))
            {
                string[] path = file.Split(Path.DirectorySeparatorChar); 
                if (path[1] == "gfx")
                {
                    assets[$"{ Path.Combine(path[path.Length - 2]) }/{Path.GetFileNameWithoutExtension(file)}"] = Image.FromFile(file);
                }
            }
            
            foreach (var subDirectory in Directory.GetDirectories(_directory))
            {
                LoadAssets(subDirectory);
            }
        }
        
        public Dictionary<string, object> Assets => assets;
        
        private static Dictionary<string, object> assets = new Dictionary<string, object>();
    }
}
