﻿namespace Icarus.Core.Notification
{
    public sealed class EventNotify
    {
        public delegate void VisDelegate();

        public event VisDelegate VisRender;

        public void Dispose()
        {
            VisRender = null;
        }

        internal void InvokeVisRender()
        {
            VisRender?.Invoke();
        }
    }
}