﻿using System;
using Icarus.Core.Notification;
using Icarus.Core.Scene;

namespace Icarus.Core
{
    public sealed class Game
    {
        internal void Update(TimeSpan _delta)
        {
            sceneManager.Update(_delta);
        }

        private void TrySwitchScene(string _trigger, string _source)
        {
            sceneManager.TrySwitchScene(_trigger, _source);
        }

        internal void StartGame()
        {
            TrySwitchScene("toStart", null);
        }

        private static readonly SceneManager sceneManager = new SceneManager();
        private static readonly EventNotify notify = new EventNotify();
    }
}