﻿using System;
using System.Collections.Generic;

namespace Icarus.Core.Scene
{
    public abstract class AbstractScene
    {
        protected AbstractScene(string _name)
        {
            name = _name;
        }

        public string Name => name;
        public abstract void Setup();
        public abstract void TearDown();
        public abstract void Resume();
        public abstract void Update(TimeSpan _delta);

        private string name;
    }
}