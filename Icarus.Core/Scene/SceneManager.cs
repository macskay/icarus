﻿using System;
using System.Collections.Generic;

namespace Icarus.Core.Scene
{
    public sealed class SceneManager
    {
        public void RegisterScene(AbstractScene _abstractScene)
        {
            scenes.Add(_abstractScene.Name, _abstractScene);
        }

        internal void AddSceneTransition(string _trigger, string _source, string _destination, string _method)
        {
            stateMachine.AddTransition(_trigger, _source, _destination, _method);
        }

        public void Update(TimeSpan _delta)
        {
            CurrentScene.Update(_delta);
        }

        public void TrySwitchScene(string _trigger, string _source)
        {
            var destMethod = stateMachine.ChangeState(_trigger, _source);
            switch (destMethod.Item2)
            {
                case "push":
                    PushScene(destMethod.Item1);
                    break;
                case "pop":
                    PopScene();
                    break;
            }
        }

        private void PushScene(string _name)
        {
            var scene = scenes[_name];
            stack.Push(scene);
        }

        private void PopScene()
        {
            var scene = stack.Pop();
            scene.TearDown();
            if (stack.Count > 0)
            {
                CurrentScene.Resume();
            }
            else
            {
                Environment.Exit(0);
            }
        }

        public AbstractScene CurrentScene => stack.Peek();
        private static readonly Dictionary<string, AbstractScene> scenes = new Dictionary<String,AbstractScene>();
        private static readonly Stack<AbstractScene> stack = new Stack<AbstractScene>();
        private static readonly SceneStateMachine stateMachine = new SceneStateMachine();
    }
}