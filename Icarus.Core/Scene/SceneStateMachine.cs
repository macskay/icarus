﻿using System;
using System.Collections.Generic;

namespace Icarus.Core.Scene
{
    internal class SceneStateMachine
    {
        public SceneStateMachine()
        {
            transitions = new List<Dictionary<string, string>>();
        }

        internal void AddTransition(string _trigger, string _source, string _destination, string _method)
        {
            if (transitions.Count == 0 && _trigger != "toStart")
            {
                throw new FirstTransitionException($"The first transition trigger to be 'toStart' in order for the framework to find the initial scene");
            }
            transitions.Add(CreateTransitionDict(_trigger, _source, _destination, _method));
        }

        private static Dictionary<string, string> CreateTransitionDict(string _trigger, string _source, string _destination,
            string _method)
        {
            return new Dictionary<string, string>
            {
                {"trigger", _trigger}, {"source", _source}, {"destination", _destination}, {"method", _method}
            };
        }

        public Tuple<string, string> ChangeState(string _trigger, string _source)
        {
            foreach (var transition in transitions)
            {
                if (_trigger != transition["trigger"]) continue;
                if (_source != transition["source"]) continue;
                return Tuple.Create(transition["destination"], transition["method"]);
            }

            throw new TransitionNotAllowedException($"This transition is not allowed: {_source} => {_trigger}");
        }

        private readonly List<Dictionary<string, string>> transitions;
    }
    
    internal sealed class TransitionNotAllowedException : Exception
    {
        public TransitionNotAllowedException(string _message) : base(_message) {}
    }

    internal sealed class FirstTransitionException : Exception
    {
        public FirstTransitionException(string _message) : base(_message) {}
    }
}