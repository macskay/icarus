﻿using System;
using System.Collections.Generic;
using System.Timers;
using Icarus.Core;
using Icarus.Core.Scene;
using Koboldstein.CLI.Vis.Scene;
using Koboldstein.Logic.Scene;

namespace Koboldstein.CLI
{
    public class MainCLI
    {
        public MainCLI()
        {
            game = new Game();
            sceneManager = new SceneManager();
            lastUpdated = new DateTime();
            logicToVis = new Dictionary<LogicSceneAbstract, VisAbstract>();
            
            SetupScenes();
        }
        
        public void Run()
        {
            StartGame();
        }

        private void StartGame()
        {
            game.StartGame();
            RunMainloop();
        }
        
        private void SetupScenes()
        {
            var logicScene = new LogicSceneGame("Game");
            CreateScene(logicScene, "toStart", null, "push");
            logicToVis.Add(logicScene, new VisGame(logicScene));
        }
        
        private void CreateScene(LogicSceneAbstract _scene, string _trigger, string _source, string _method)
        {
            sceneManager.RegisterScene(_scene);
            sceneManager.AddSceneTransition(_trigger, _source, _scene.Name, _method);
        }

        private void RunMainloop()
        {
            ResetRenderDelta();
            
            while (true)
            {
                var visScene = logicToVis[(LogicSceneAbstract) sceneManager.CurrentScene];
                Update(visScene, Delta);
                Render(visScene);
            }
        }

        private void Update(VisAbstract _visScene, TimeSpan _delta)
        {
            game.Update(_delta);
            _visScene.Update(_delta);
        }

        private void Render(VisAbstract _visScene)
        {
            _visScene.Render();
            ResetRenderDelta();
        }

        private void ResetRenderDelta()
        {
            renderDelta = TimeSpan.Zero;
        }
        
        private static TimeSpan Delta
        {
            get
            {
                var now = DateTime.Now;
                var delta = now - lastUpdated;
                lastUpdated = now;
                renderDelta += delta;
                
                return delta;
            }
        }

        private static Game game;
        private static SceneManager sceneManager; 
        private static Dictionary<LogicSceneAbstract, VisAbstract> logicToVis;
        
        private static DateTime lastUpdated;
        private static TimeSpan renderDelta;
    }
}