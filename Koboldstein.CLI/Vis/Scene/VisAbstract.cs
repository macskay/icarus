﻿using System;

namespace Koboldstein.CLI.Vis.Scene
{
    public abstract class VisAbstract
    {
        public abstract void Render();

        public abstract void Update(TimeSpan _delta);
    }
}