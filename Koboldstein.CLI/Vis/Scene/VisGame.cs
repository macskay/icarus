﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Icarus.Core;
using Icarus.Core.Scene;
using Koboldstein.Logic.Scene;

namespace Koboldstein.CLI.Vis.Scene
{
    public class VisGame : VisAbstract
    {
        public VisGame(AbstractScene _logicScene)
        {
            logicScene = (LogicSceneGame) _logicScene;
        }

        public override void Update(TimeSpan _delta)
        {
            if (logicScene.CheckWinCondition())
            {
                return;
            }
            
            var randomPan = GetSelectedPanRandomly();
            logicScene.TakeTurn(randomPan);
        }

        private int GetSelectedPanRandomly()
        {
            List<int> validPans = new List<int>();
            var currentPlayerId = logicScene.CurrentPlayerId;
            var bottomRow = logicScene.Factions[currentPlayerId].Board.Pans.Take(8).ToList();
            for (int i = 0; i < bottomRow.Count; ++i)
            {
                if (bottomRow[i] > 1)
                {
                    validPans.Add(i);
                }
            }
            int randomPan = new Random().Next(validPans.Count);
            return validPans[randomPan];
        }
        
        public override void Render()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Turn: {logicScene.Turn}");
            foreach (var faction in logicScene.Factions)
            {
                sb.AppendLine($"Faction {faction.Id}: {String.Join(", ", faction.Board.Pans)}");
            }
            Console.WriteLine(sb.ToString());
            Console.WriteLine("");
            if (logicScene.GameOver)
            {
                Console.WriteLine($"Game is over (Turn: {logicScene.Turn}). Faction {logicScene.Winner} won!");
                Environment.Exit(0);
            }
        }

        private readonly LogicSceneGame logicScene;
    }
}