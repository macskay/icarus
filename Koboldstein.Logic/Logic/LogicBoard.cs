﻿using System.Collections.Generic;

namespace Koboldstein.Logic
{
    public class LogicBoard
    {
        public LogicBoard()
        {
            Setup();
        }

        private void Setup()
        {
            SetupPans();
        }

        private void SetupPans()
        {
            Pans = new List<int>();
            for (int i = 0; i < NumberOfPans; ++i)
            {
                Pans.Add(START_STONES);
            }
        }

        public int TakeStones(int _panId)
        {
            var panId = _panId % Pans.Count;
            var stones = Pans[panId];
            Pans[panId] = 0;
            
            return stones;
        }

        public int GetStoneCountFromPan(int _panId)
        {
            return Pans[_panId % Pans.Count];
        }
        
        public void MoveStones(int _stonesTaken, int _startPan)
        {
            for (int i = 1; i <= _stonesTaken; ++i) 
            {
                var currentPan = (_startPan + i) % Pans.Count;
                ++Pans[currentPan];
            }
        }

        private int GetPanIndexInRow(int _panId)
        {
            return _panId % NUMBER_OF_STONES_PER_ROW;
        }
        
        internal int GetDestinationStealPan(int _stealSourcePan)
        {
            return NumberOfPans - GetPanIndexInRow(_stealSourcePan) - 1;
        }
        
        internal static bool IsPanInSecondRow(int _startPan)
        {
            return _startPan >= NUMBER_OF_STONES_PER_ROW;
        }
        
        internal bool EndPanHasTwoOrMoreStones(int _endPan)
        {
            return GetStoneCountFromPan(_endPan) > 1;
        }

        private int NumberOfPans => NUMBER_OF_ROWS * NUMBER_OF_STONES_PER_ROW;
        public List<int> Pans { get; private set; }

        private const int START_STONES = 2;
        public const int NUMBER_OF_STONES_PER_ROW = 8;
        private const int NUMBER_OF_ROWS = 2;

    }
}