﻿using System.Data;
using Koboldstein.Logic.Scene;

namespace Koboldstein.Logic
{
    public class LogicFaction
    {
        public LogicFaction(LogicSceneGame _game, int _id)
        {
            id = _id;
            game = _game;
            board = new LogicBoard();
        }

        public void TakeTurn(int _startPan)
        {
            while (true)
            {
                var stonesTaken = board.TakeStones(_startPan);
                var stonesStolen = TryStealing(_startPan);
                var totalStones = stonesTaken + stonesStolen;

                board.MoveStones(totalStones, _startPan);

                var endPan = (_startPan + totalStones) % board.Pans.Count;

                if (!board.EndPanHasTwoOrMoreStones(endPan) || game.CheckWinCondition())
                {
                    return;
                }

                _startPan = endPan;
            }
        }

        private int TryStealing(int _startPan)
        {
            return LogicBoard.IsPanInSecondRow(_startPan) ? StealStones(_startPan) : 0; 
        }


        private int StealStones(int _stealSourcePan)
        {
            var otherFaction = game.Factions[game.GetOtherFactionId(Id)];
            var stealPan = board.GetDestinationStealPan(_stealSourcePan);
            
            return otherFaction.Board.TakeStones(stealPan);
        }
        
        

        public LogicBoard Board => board;

        public int Id => id;
        
        private int id;
        private LogicBoard board;
        private LogicSceneGame game;
    }
}