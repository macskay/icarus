﻿using Icarus.Core.Scene;

namespace Koboldstein.Logic.Scene
{
    public abstract class LogicSceneAbstract : AbstractScene
    {
        protected LogicSceneAbstract(string _name) : base(_name)
        {
        }
    }
}
