﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Koboldstein.Logic.Scene
{
    public sealed class LogicSceneGame : LogicSceneAbstract
    {
        public LogicSceneGame(string _name) : base(_name)
        {
            CurrentPlayerId = 0;
            Turn = 0;

            Setup();
        }

        public override void Setup()
        {
            SetupFactions();
        }

        private void SetupFactions()
        {
            factions = new List<LogicFaction>();
            for (int i = 0; i < NUMBER_OF_FACTIONS; ++i)
            {
                factions.Add(new LogicFaction(this, i));
            }
        }

        public override void TearDown()
        {
        }

        public override void Resume()
        {
        }

        public override void Update(TimeSpan _delta)
        {
            CheckWinCondition();
        }

        internal void TakeTurn(int _troughId)
        {
            if (_troughId > 7)
            {
                Console.Error.WriteLine("False Trough Id. Needs to be between 0 and 7.");
                return;
            }

            factions[CurrentPlayerId].TakeTurn(_troughId);

            CurrentPlayerId = GetOtherFactionId(CurrentPlayerId); // Maybe change to bitwise change
            ++Turn;
        }

        private bool HasFactionWon(out int _winnerId)
        {
            _winnerId = -1;
            foreach (var faction in Factions)
            {
                var pans = faction.Board.Pans;
                var topRow = pans.Skip(Math.Max(0, pans.Count) - 8);
                var bottomRow = pans.Take(8);

                var topRowEmpty = topRow.All(_x => _x == 0);
                var bottomRowInvalid = bottomRow.All(_x => _x < 2);

                if (!topRowEmpty && !bottomRowInvalid)
                {
                    continue;
                }
                
                _winnerId = GetOtherFactionId(faction.Id);
                return true;
            }
            
            return false;
        }

        public int GetOtherFactionId(int _factionId)
        {
            return _factionId == 1 ? 0 : 1;
        }
        
        public bool CheckWinCondition()
        {
            if (!HasFactionWon(out int winnerId))
            {
                return false;
            }
            GameOver = true;
            Winner = winnerId;
            
            return true;
        }
        
        public List<LogicFaction> Factions => factions;
        public bool GameOver { get; private set; }
        public int Winner { get; private set; }
        public int Turn { get; private set; }
        public int CurrentPlayerId { get; private set; }

        private static List<LogicFaction> factions;
        
        private const int NUMBER_OF_FACTIONS = 2;
    }
}