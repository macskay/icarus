﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Koboldstein.Logic.Scene
{
    public class LogicSceneSplash : LogicSceneAbstract
    {
        public LogicSceneSplash(string _name) : base(_name)
        {
        }

        public override void Setup()
        {
        }

        public override void TearDown()
        {
        }

        public override void Resume()
        {
        }

        public override void Update(TimeSpan _delta)
        {
        }

    }
}
