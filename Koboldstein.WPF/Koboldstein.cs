﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Icarus.Asset;
using Icarus.Core;
using Icarus.Core.Math;
using Icarus.Core.Scene;
using Koboldstein.Logic.Scene;
using Koboldstein.WPF.Vis.Scene;

namespace Koboldstein.WPF
{
    public partial class KoboldsteinForm : Form
    {
        public KoboldsteinForm()
        {
            assetManager = new AssetManager(); 
            game = new Game();
            sceneManager = new SceneManager();
            logicToVis = new Dictionary<LogicSceneAbstract, VisAbstract>();
            
            InitializeComponent();
            
            SetupScenes();
            SetupTimer();
            StartGame();
        }

        private void SetupTimer()
        {
            timer = new Timer() { Interval = (int)  (1f / FPS * 1000f), Enabled = true };
            timer.Tick += Update;
        }

        private void StartGame()
        {
            timer.Start();
            game.StartGame();
        }

        private void SetupScenes()
        {
            var logicScene = new LogicSceneGame("Game");
            CreateScene(logicScene, "toStart", null, "push");
            logicToVis.Add(logicScene, new VisGame(logicScene, assetManager.Assets, this));
        }

        private void CreateScene(LogicSceneAbstract _scene, string _trigger, string _source, string _method)
        {
            sceneManager.RegisterScene(_scene);
            sceneManager.AddSceneTransition(_trigger, _source, _scene.Name, _method);
        }

        private void Update(object _sender, EventArgs _event)
        {
            var now = DateTime.Now;
            var delta = now - lastUpdated;
            lastUpdated = now;
            
            game.Update(delta);
            Invalidate();
        }

        private void Render(object _sender, PaintEventArgs _event)
        {
            var visScene = logicToVis[(LogicSceneAbstract) sceneManager.CurrentScene];
            visScene.Render(_event);
        }

        private readonly AssetManager assetManager;
        private readonly Game game; 
        private readonly SceneManager sceneManager;
        private readonly Dictionary<LogicSceneAbstract, VisAbstract> logicToVis;
        
        private Timer timer;
        private DateTime lastUpdated;

        internal static readonly Vector2 windowSize = new Vector2(1600, 1036);
        private const float FPS = 60.0f;
    }
}