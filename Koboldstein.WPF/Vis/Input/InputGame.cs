﻿using System;
using System.Windows.Forms;
using Koboldstein.WPF.Vis.Scene;

namespace Koboldstein.WPF.Vis.Input
{
    public class InputGame : InputGeneral
    {
        public InputGame(VisGame _visGame)
        {
            visGame = _visGame;
        }

        public override void KeyUp(object _sender, KeyEventArgs _keyEventArgs)
        {
            base.KeyUp(_sender, _keyEventArgs); 
        }

        public override void MouseUp(object _sender, MouseEventArgs _e)
        {
            visGame.CollisionDetection(_e.Location);
        }

        private VisGame visGame;
    }
}