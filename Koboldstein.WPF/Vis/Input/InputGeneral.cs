﻿using System;
using System.Windows.Forms;

namespace Koboldstein.WPF.Vis.Input
{
    public class InputGeneral
    {
        public virtual void KeyUp(object _sender, KeyEventArgs _keyEventArgs)
        {
            Console.WriteLine("Key up - InputGeneral");
            switch (_keyEventArgs.KeyCode)
            {
                case Keys.Escape:
                {
                    Environment.Exit(-1);
                    break;
                }
            }
        }

        public virtual void MouseUp(object _sender, MouseEventArgs _e)
        {
        }
    }
}