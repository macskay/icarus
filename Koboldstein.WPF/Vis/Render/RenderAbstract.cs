﻿using System.Drawing;
using System.Windows.Forms;
using Icarus.Core.Math;
using Koboldstein.Logic.Scene;

namespace Koboldstein.WPF.Vis.Render
{
    public abstract class RenderAbstract
    {
        protected RenderAbstract(Image _image, LogicSceneGame _game)
        {
            image = _image;
            game = _game;
        }

        public virtual void Render(PaintEventArgs _event)
        {
            _event.Graphics.DrawImage(image, Position.X, Position.Y, image.Width, image.Height);
        }

        protected Image image;
        public Vector2 Position { get; set; }
        protected LogicSceneGame game;
    }
}