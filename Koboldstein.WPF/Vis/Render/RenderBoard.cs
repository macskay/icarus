﻿using System.Drawing;
using Icarus.Core.Math;
using Koboldstein.Logic.Scene;

namespace Koboldstein.WPF.Vis.Render
{
    public class RenderBoard : RenderAbstract
    {
        public RenderBoard(Image _image, LogicSceneGame _game) : base(_image, _game)
        {
            Position = Vector2.Zero;
        }
    }
}