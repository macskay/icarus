﻿using System.Drawing;
using System.Drawing.Text;
using System.Windows.Forms;
using Icarus.Core.Math;
using Koboldstein.Logic;
using Koboldstein.Logic.Scene;

namespace Koboldstein.WPF.Vis.Render
{
    public class RenderPan : RenderAbstract
    {
        public RenderPan(Image _image, LogicSceneGame _game, int _factionId, int _panIndex) : base(_image, _game)
        {
            factionId = _factionId;
            index = _panIndex;

            Position = ComputePosition();
        }

        private Vector2 ComputePosition()
        {
            if (factionId == 1) // other player
            {
                var x = (!LogicBoard.IsPanInSecondRow(index)) ? ForwardX() : BackwardX();
                var y = START_Y + MARGIN_GENERAL + (LogicBoard.IsPanInSecondRow(index) ? 1 : 0) * (image.Height + MARGIN_GENERAL + MARGIN_Y + MARGIN_GENERAL);
                return new Vector2(x, y);
            }
            else
            {
                var x = (!LogicBoard.IsPanInSecondRow(index)) ? BackwardX() : ForwardX();
                var y = ((int) KoboldsteinForm.windowSize.Y - image.Height) - (START_Y + MARGIN_GENERAL + (LogicBoard.IsPanInSecondRow(index) ? 1 : 0) * (image.Height + MARGIN_GENERAL + MARGIN_Y + MARGIN_GENERAL));
                return new Vector2(x, y);
            }
        }

        private int BackwardX()
        {
            return ((int) KoboldsteinForm.windowSize.X - image.Width) - (START_X + MARGIN_GENERAL + index % LogicBoard.NUMBER_OF_STONES_PER_ROW * (image.Width + MARGIN_GENERAL + MARGIN_GENERAL));
        }

        private int ForwardX()
        {
            return START_X + MARGIN_GENERAL + index % LogicBoard.NUMBER_OF_STONES_PER_ROW * (image.Width + MARGIN_GENERAL + MARGIN_GENERAL);
        }


        public override void Render(PaintEventArgs _event)
        {
            base.Render(_event);
            
            /* For debugging */
            _event.Graphics.DrawString(index.ToString(), SystemFonts.DefaultFont, new SolidBrush(Color.Red), Position.X, Position.Y);

            RenderStoneText(_event);
        }

        private void RenderStoneText(PaintEventArgs _event)
        {
            var stones = game.Factions[factionId].Board.Pans[index];
            var textSize = _event.Graphics.MeasureString(stones.ToString(), SystemFonts.DefaultFont); 
            var x = Position.X + image.Width / 2.0f - textSize.Width / 2.0f;
            var y = Position.Y + image.Height / 2.0f - textSize.Height / 2.0f;
            _event.Graphics.DrawString(stones.ToString(), SystemFonts.DefaultFont, new SolidBrush(Color.White), x, y);
        }

        public int Width => image.Width;
        public int Height => image.Height;

        private int factionId;
        private int index;

        private const int MARGIN_GENERAL = 5;
        private const int MARGIN_Y = 22;
        private const int START_X = 20;
        private const int START_Y = 42;
    }
}