﻿using System;
using System.Windows.Forms;
using Koboldstein.WPF.Vis.Input;

namespace Koboldstein.WPF.Vis.Scene
{
    public abstract class VisAbstract
    {
        protected VisAbstract(Form _form)
        {
            form = _form;
        }

        public abstract void Update(TimeSpan _delta);
        
        public abstract void Render(PaintEventArgs _event);

        protected readonly Form form;
        protected InputGeneral InputGeneral;
    }
}