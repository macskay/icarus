﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using Icarus.Core;
using Icarus.Core.Math;
using Icarus.Core.Scene;
using Koboldstein.Logic.Scene;
using Koboldstein.WPF.Vis.Input;
using Koboldstein.WPF.Vis.Render;

namespace Koboldstein.WPF.Vis.Scene
{
    public class VisGame : VisAbstract
    {
        public VisGame(AbstractScene _logicScene, Dictionary<string, object> _assets, Form _form) : base(_form)
        {
            logicScene = (LogicSceneGame) _logicScene;
            InputGeneral = new InputGame(this);
            assets = _assets;

            SetupDelegates();
            SetupRenders();
        }

        private void SetupDelegates()
        {
            form.KeyUp += InputGeneral.KeyUp;
            form.MouseUp += InputGeneral.MouseUp;
        }

        private void SetupRenders()
        {
            LoadBoard();
            LoadPans();
        }

        private void LoadBoard()
        {
            backdrop = new RenderBoard((Image) assets["game/board"], logicScene);
        }

        private void LoadPans()
        {
            pans = new List<RenderPan>();

            foreach (var faction in logicScene.Factions)
            {
                for (int i = 0; i < faction.Board.Pans.Count; ++i)
                {
                    pans.Add(new RenderPan((Image) assets["game/pan"], logicScene, faction.Id, i));
                }
            }
        }


        public override void Update(TimeSpan _delta)
        {
            
        }

        public override void Render(PaintEventArgs _event)
        {
            _event.Graphics.Clear(Color.Black);
            _event.Graphics.ResetTransform();
            
            // _event.Graphics.DrawImage(backdrop, 0, form.Height / 2 - backdrop.Height / 2, form.Width,
            //    backdrop.Height);
            backdrop.Render(_event);
            foreach (var pan in pans)
            {
                var center = pan.Position + new Vector2(pan.Width / 2.0f, pan.Height / 2.0f);
                var size = 10;
                _event.Graphics.DrawEllipse(new Pen(new SolidBrush(Color.Red)), new Rectangle((int) center.X - (size / 2), (int) center.Y - (size / 2), size, size));
                pan.Render(_event); 
            }
        }
        
        public void CollisionDetection(Point _eLocation)
        {
            if (LocationInsidePan(_eLocation, out var _hitPan))
            {
                Console.WriteLine($"Hit Pan: {_hitPan.Position}"); 
            }
        }

        private bool LocationInsidePan(Point _eLocation, out RenderPan _hitPan)
        {
            foreach (var renderPan in pans)
            {
                var center = renderPan.Position + new Vector2(renderPan.Width / 2.0f, renderPan.Height / 2.0f);
                var radius = renderPan.Width;
                if (Math.Pow((_eLocation.X - center.X), 2) + Math.Pow((_eLocation.Y - center.Y), 2) <
                    Math.Pow(radius, 2))
                {
                    _hitPan = renderPan;
                    return true;
                }
            }

            _hitPan = null;
            return false;
        }

        private readonly LogicSceneGame logicScene;
        
        private RenderBoard backdrop;
        private List<RenderPan> pans;

        private readonly Dictionary<string, object> assets;
    }
}