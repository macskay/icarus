﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Icarus.Core.Scene;
using Koboldstein.Logic.Scene;

namespace Koboldstein.WPF.Vis.Scene
{
    public class VisSplash : VisAbstract
    {
        public VisSplash(AbstractScene _logicScene, Dictionary<string, object> _assets, Form _form) : base(_form)
        {
            logicSceneScene = (LogicSceneSplash) _logicScene;
            
            ExtractAssets(_assets);
        }

        private void ExtractAssets(Dictionary<string, object> _assets)
        {
            backdrop = (Image) _assets["splash/backdrop"];
        }

        public override void Update(TimeSpan _delta)
        {
        }

        public override void Render(PaintEventArgs _event)
        {
            _event.Graphics.Clear(Color.Black);
            _event.Graphics.ResetTransform();
            
            _event.Graphics.DrawImage(backdrop, 0, form.Height / 2 - backdrop.Height / 2, form.Width,
                backdrop.Height);
        }

        private readonly LogicSceneSplash logicSceneScene;
        
        private Image backdrop;
    }
}